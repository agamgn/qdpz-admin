module.exports = {
	messages: {
		CN: {
			postName: '岗位',
			post:'web前端工程师',
			workPlaceName: '工作地点',
			workTimeName: '工作经验',
			userName: '姓名',
			isName: '周凯文'
		},
		HK: {
			postName: '崗位',
			post:'web前端工程師',
			workPlaceName: '工作地點',
			workTimeName: '工作經驗',
			userName: '姓名',
			isName: '周凱文'
		},
		US: {
			postName: 'PostName',
			post:'Web front end Engineer',
			workPlaceName: 'work Place',
			workTimeName: 'work Time',
			userName: 'userName',
			isName: 'Kevin Chou'
		},
	}
}
