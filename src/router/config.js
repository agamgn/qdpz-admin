import TabsView from '@/layouts/tabs/TabsView'
import BlankView from '@/layouts/BlankView'
import PageView from '@/layouts/PageView'

// 路由配置
const options = {
	routes: [{
			path: '/login',
			name: '登录页',
			component: () => import('@/pages/login')
		},
		{
			path: '*',
			name: '404',
			component: () => import('@/pages/exception/404'),
		},
		{
			path: '/403',
			name: '403',
			component: () => import('@/pages/exception/403'),
		},
		{
			path: '/',
			name: '首页',
			component: TabsView,
			redirect: '/login',
			children: [{
					path: 'dashboard',
					name: '从这里开始吧',
					meta: {
						icon: 'smile'
					},
					component: BlankView,
					children: [{
							path: 'workplace',
							name: '首页',
							meta: {
								page: {
									closable: false
								}
							},
							component: () => import('@/pages/dashboard/workplace'),
						},
						{
							path: 'aboutme',
							name: '关于我',
							component: () => import('@/pages/dashboard/aboutme'),
						}
					]
				},
				{
					path: 'qdpz',
					name: '前端铺子 · 管理',
					meta: {
						icon: 'laptop',
					},
					component: BlankView,
					children: [
						// {
						// 	path: 'Demo',
						// 	name: '测试页面',
						// 	component: () => import('@/pages/demo/Demo')
						// },
						{
							path: 'blog',
							name: '文章管理',
							component: () => import('@/pages/qdpz/blog/blogList/BlogList')
						},
						{
							path: 'project',
							name: '项目管理',
							component: () => import('@/pages/qdpz/project/projectList/ProjectList')
						},
						{
							path: 'consProject',
							name: '寄售管理',
							component: () => import('@/pages/qdpz/consProject/index')
						}
					]
				},
				{
					name: '个人博客',
					path: 'blog',
					meta: {
						icon: 'laptop',
					},
					component: () => import('@/pages/blog/blog')
				},
				{
					name: '个人博客详情',
					path: 'blogContent',
					meta: {
						icon: 'laptop',
					},
					component: () => import('@/pages/blog/blogContent')
				},
				{
					name: '生活记录',
					path: 'life',
					meta: {
						icon: 'dashboard',
					},
					component: () => import('@/pages/life/life')
				},
				{
					path: 'kevin_components',
					name: 'UI组件模版',
					meta: {
						icon: 'warning',
					},
					component: BlankView,
					children: [{
							path: 'Position',
							name: '位置监控',
							component: () => import('@/pages/kevin_components/map/Position')
						}
					]
				},
				{
					path: 'form',
					name: '开发组件合集',
					meta: {
						icon: 'form',
						page: {
							cacheAble: false
						}
					},
					component: PageView,
					children: [{
							path: 'basic',
							name: '基础表单',
							component: () => import('@/pages/form/basic'),
						},
						{
							path: 'step',
							name: '分步表单',
							component: () => import('@/pages/form/step'),
						},
						{
							path: 'advance',
							name: '高级表单',
							component: () => import('@/pages/form/advance'),
						},
						{
							path: 'query',
							name: '查询表格',
							meta: {
								authority: 'queryForm',
							},
							component: () => import('@/pages/list/QueryList'),
						},
						{
							path: 'query/detail/:id',
							name: '查询详情',
							meta: {
								highlight: '/list/query',
								invisible: true
							},
							component: () => import('@/pages/Demo')
						},
						{
							path: 'primary',
							name: '标准列表',
							component: () => import('@/pages/list/StandardList'),
						},
						{
							path: 'card',
							name: '卡片列表',
							component: () => import('@/pages/list/CardList'),
						},
						{
							path: 'search',
							name: '搜索列表',
							component: () => import('@/pages/list/search/SearchLayout'),
							children: [{
									path: 'article',
									name: '文章',
									component: () => import('@/pages/list/search/ArticleList'),
								},
								{
									path: 'application',
									name: '应用',
									component: () => import('@/pages/list/search/ApplicationList'),
								},
								{
									path: 'project',
									name: '项目',
									component: () => import('@/pages/list/search/ProjectList'),
								}
							]
						},
						{
							path: 'taskCard',
							name: '任务卡片',
							component: () => import('@/pages/components/TaskCard')
						},
						{
							path: 'palette',
							name: '颜色复选框',
							component: () => import('@/pages/components/Palette')
						},
						{
							path: 'table',
							name: '高级表格',
							component: () => import('@/pages/components/table')
						}
					]
				},
				{
					path: 'details',
					name: '详情页模版',
					meta: {
						icon: 'profile'
					},
					component: BlankView,
					children: [{
							path: 'analysis',
							name: '图表分析页',
							component: () => import('@/pages/dashboard/analysis'),
						},
						{
							path: 'basic',
							name: '基础详情页',
							component: () => import('@/pages/detail/BasicDetail')
						},
						{
							path: 'advance',
							name: '高级详情页',
							component: () => import('@/pages/detail/AdvancedDetail')
						},
						{
							path: 'success',
							name: '成功页面',
							component: () => import('@/pages/result/Success')
						},
						{
							path: 'error',
							name: '成功页面',
							component: () => import('@/pages/result/Error')
						}
					]
				},
				{
					path: 'exception',
					name: '异常页模版',
					meta: {
						icon: 'warning',
					},
					component: BlankView,
					children: [{
							path: '404',
							name: 'Exp404',
							component: () => import('@/pages/exception/404')
						},
						{
							path: '403',
							name: 'Exp403',
							component: () => import('@/pages/exception/403')
						},
						{
							path: '500',
							name: 'Exp500',
							component: () => import('@/pages/exception/500')
						}
					]
				},
				{
					name: '验权表单',
					path: 'auth/form',
					meta: {
						icon: 'file-excel',
						authority: {
							permission: 'form'
						}
					},
					component: () => import('@/pages/form/basic')
				},
				{
					name: 'Gitee地址',
					path: 'kevin',
					meta: {
						icon: 'github',
						link: 'https://gitee.com/kevin_chou'
					}
				}
			]
		},
	]
}

export default options
