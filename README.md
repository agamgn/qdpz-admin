<p align="center">
    <img width="200" src="https://cdn.zhoukaiwen.com/logo.png">
</p>

<p align="center">
	<a href="https://gitee.com/kevin_chou/qdpz-admin/stargazers" target="_blank">
		<img src="https://svg.hamm.cn/gitee.svg?type=star&user=kevin_chou&project=qdpz-admin"/>
	</a>
	<a href="https://gitee.com/kevin_chou/qdpz-admin/members" target="_blank">
		<img src="https://svg.hamm.cn/gitee.svg?type=fork&user=kevin_chou&project=qdpz-admin"/>
	</a>
	<img src="https://svg.hamm.cn/badge.svg?key=Platform&value=微信小程序"/>
</p>

<h1 align="center">《前端铺子》· Vue后台管理系统 </h1>

<div align="center">


<p>基于Vue，使用Ant Design Vue的Ui框架</p>

```
🕙 项目基本保持每日更新，右上随手点个 🌟 Star 关注，这样才有持续下去的动力，谢谢～
```
</div>


> 目前该项目还在持续开发中,欢迎您持续关注我们

## 👨‍💻‍项目介绍

---

前端铺子后台管理系统基于 Vue2.x + JavaScript + Ant Design Vue
等最新的技术栈开源的企业级后台管理系统,我们的初衷是不断的更新迭代使用最新的技术栈创建完善的/简单的/快速的后台管理系统
使用最为宽松的MIT开源协议,无需任何授权即可免费商用,希望可以减少大家重复代码的时间,多去陪陪家人,陪陪孩子!

### 🚀项目架构

---

+ 后端使用NodeJS+MySQL开发,适合新手,完全遵循代码规范,性能好~
+ 前后端完全分离,后端遵循RESTful API风格开发
+ 前端基于Vue2.x 完全使用Javascript
+ 前端框架使用Ant Design Vue 版本简洁大气美观

> 未来根据时间计划,还会增加[php语言版本][Python语言版本][GOLang语言版本]等等!

### 💥软件功能

---

+ 已完成功能
    - [x] 后台首页 包含了:便捷导航/项目总览/常用技术栈等内容.
    - [x] 关于我们 对开发作者的简单介绍,页面可替换数据.
    - [x] 文章管理 对于开源项目[前端铺子]的文章的增删改查点赞等功能.
    - [x] 项目管理 对于开源项目[前端铺子]的项目的增删改查点赞等功能.
    - [x] 组件模板 引入地图及地图相关操作.
    - [x] 附件管理 静态资源可上传至七牛云,管理当前系统上传的文件及图片等信息.

+ 待完成功能
    - [ ] 消息公告 管理前端的公告消息等.
    - [ ] 个人中心 同步数据.

### 🌴环境要求

---

+ 服务器推荐使用宝塔面板管理服务器环境
+ 本地需安装node,npm

### 📝安装教程

---

1. 安装后台管理系统

### yarn
```bash
$ yarn install
$ yarn serve
```

### or npm
```
$ npm install
$ npm run serve
```

2. 安装前端

+ 本项目为前后端分离开发,请移步前端仓库
    + [Gitee 前端铺子-uniapp移动端](https://gitee.com/kevin_chou/qdpz)


3. 安装NodeJS

+ 本项目为前后端分离开发,请移步后端仓库
    + [Gitee 前端铺子-NodeJS后端](https://gitee.com/kevin_chou/qdpz-nodejs)

### 🎉文档演示

---

> 正在完善中... 
> qdpz.zhoukaiwen.com

### 🌈截图预览

---

<table>
    <tr>
        <td><img src="public/mdImg/demo1.png"></td>
        <td><img src="public/mdImg/demo2.png"></td>
    </tr>
    <tr>
        <td><img src="public/mdImg/demo3.png"></td>
        <td><img src="public/mdImg/demo4.png"></td>
    </tr>
</table>

### 💕特别鸣谢

---

> 感谢以下开源团队

+ [Antd Design](https://ant.design/)



### 👍「前端铺子」交流群

---

<p>一群人数已500人满，大家可扫码加我拉群，请备注：gitee</p>
<p>已加入大佬：naive-ui-admin啊俊、图鸟-可我会像、TopicQ作者等等前后端全栈大佬</p>
<p>
<img src="https://zhoukaiwen.com/img/WechatIMG1320.jpeg" width="200px" />
<img src="https://cdn.zhoukaiwen.com/qdpz_jt2.jpg" width="200px" />
<!-- <img src="https://cdn.zhoukaiwen.com/xhd_wx.jpg" width="300px" /> -->
<img src="https://zhoukaiwen.com/img/kevin_wx_jt.jpg" width="200px" />

</p>

### 🔐版权信息

---

> [前端铺子] 遵循MIT开源协议发布,并提供免费使用!

> 使用本框架不得用于开发违反国家有关政策的相关软件和应用,否则要付法律责任的哦!

### 💖支持项目

---

> 打赏就duck不必啦～ 就点点🌟 Star 🌟 关注更新，支持下作者就可以了

> 更多信息参考 [vue-antd-admin](https://iczer.gitee.io/vue-antd-admin-docs)
> 更多信息参考 [使用文档1x](https://1x.antdv.com/docs/vue/introduce-cn/)